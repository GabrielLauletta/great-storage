## Documentação de projeto

### Projeto voltado para auxilio do armazenamento.


# Instalações:
 - ## Python
   - Passo 1: Instale o python 3:
     -  sudo apt-get install python3.5

   - Passo 2: Instale o pip ( certifique-se que o python padrão é o 3, digite 'python' no terminal e veja a versão!):
     - sudo apt-get install python-pip

   - Passo 3: Caso o python padrão seja o 2 e você não queira mudar, utilize:
     - sudo apt-get install python3-pip

 - ## PyCharm
   - Passo 1: Abra o terminal (no Unity use as teclas CTRL + ALT + T):

   - Passo 2: Adicione o repositório do programa:
     - sudo add-apt-repository ppa:viktor-krivak/pycharm

   - Passo 3: Atualize o gerenciador de pacotes com o comando:
     - sudo apt-get update

   - Passo 4: instalar a IDE PyCharm básica:
     - sudo apt-get install pycharm
   
 - ## MySql
   - Passo 1: Atualize seu ambiente:
     - sudo apt update

   - Passo 2: Instale o servidor:
     - sudo apt install mysql-server

   - Passo 3: Realize a instalação segura de acordo com suas preferencias ( recomendado que saiba inglês básico ):
     - mysql_secure_installation
 
 - ## Mysql Workbench
   - Passo 1: Execute o comando:
     - sudo apt install mysql-workbench

 - ## Django versão 2.1.2
   - Documentação Django
     - https://docs.djangoproject.com/en/2.1/
   - Instalação
     - pip install Django==2.1.2